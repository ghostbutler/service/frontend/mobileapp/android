package com.example.joseph.ghostbutlerapp.fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.Switch;

import com.example.joseph.ghostbutlerapp.R;
import com.example.joseph.ghostbutlerapp.backendServices.ActionSender;
import com.example.joseph.ghostbutlerapp.dataClasses.Action;

import org.json.JSONException;
import org.w3c.dom.Text;

import java.net.URISyntaxException;
import java.security.NoSuchAlgorithmException;

public class MusicFragment extends Fragment {

    private EditText url;
    private Button buttonPlay, buttonPause, buttonStop, buttonLoad;
    private SeekBar volume;
    private int volumeValue;
    private String name;

    @SuppressLint("ClickableViewAccessibility")
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_music, container, false);

        name = getArguments().getString("name");

        url = rootView.findViewById(R.id.musicURL);
        buttonPlay = rootView.findViewById( R.id.play );
        buttonPause = rootView.findViewById( R.id.pause );
        buttonStop = rootView.findViewById( R.id.stop );
        buttonLoad = rootView.findViewById( R.id.load );
        volume = rootView.findViewById( R.id.volume2 );

        volume.setMax( 100 );
        volume.setProgress( 80 );

        buttonPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                play( );
            }
        });

        buttonPause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               pause( );
            }
        });

        buttonStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                delete( );
            }
        });

        buttonLoad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               load( );
            }
        });

        volume.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                volumeValue = i;
                play();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        return rootView;
    }



    private void play(){
        Action action = new Action("music", name);
        action.setIdentifier("play");

        action.addField("volume", volumeValue);
        action.addField("name", "runFromMobileApp.mp3" );

        try {
            ActionSender sender = new ActionSender(getString(R.string.rabbitmq_address));
            sender.publishMessage(action.toJson().toString().replace( "\\", "" ) );
            sender.publishToAMQP();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void load(){
        Action action = new Action("music", name);
        action.setIdentifier("load");
        action.addField("isMustPlay", true);
        action.addField("volume", volumeValue);
        action.addField("name", "runFromMobileApp.mp3");
        action.addField("url", url.getText());

        try {
            ActionSender sender = new ActionSender(getString(R.string.rabbitmq_address));
            sender.publishMessage(action.toJson().toString().replace( "\\", "" ) );
            sender.publishToAMQP();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void pause(){
        Action action = new Action("music", name);
        action.setIdentifier("pause");
        action.addField("name", "runFromMobileApp.mp3");

        try {
            ActionSender sender = new ActionSender(getString(R.string.rabbitmq_address));
            sender.publishMessage(action.toJson().toString().replace( "\\", "" ) );
            sender.publishToAMQP();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void delete(){
        Action action = new Action("music", name);
        action.setIdentifier("delete");
        action.addField("name", "runFromMobileApp.mp3");

        try {
            ActionSender sender = new ActionSender(getString(R.string.rabbitmq_address));
            sender.publishMessage(action.toJson().toString().replace( "\\", "" ) );
            sender.publishToAMQP();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}

