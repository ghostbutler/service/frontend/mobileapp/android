package com.example.joseph.ghostbutlerapp.Activities;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.joseph.ghostbutlerapp.Dialog.RenameDialog;
import com.example.joseph.ghostbutlerapp.R;
import com.example.joseph.ghostbutlerapp.dataClasses.ConnectedObject;
import com.example.joseph.ghostbutlerapp.fragment.LightFragment;
import com.example.joseph.ghostbutlerapp.fragment.OutletFragment;
import com.example.joseph.ghostbutlerapp.fragment.TexttospeechFragment;
import com.example.joseph.ghostbutlerapp.fragment.MusicFragment;

public class ObjectActivity extends FragmentActivity {

    private String objectType;

    private ImageView image;
    private TextView title, type;
    private Fragment fragment;
    private ConnectedObject object;

    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_object);

        object = (ConnectedObject)getIntent().getSerializableExtra("object");

        image = findViewById(R.id.object_image);
        title = findViewById(R.id.object_title);
        type = findViewById(R.id.object_type);

        setupFragment(selectCorrectFragment(object));
        setUpImage(object.getTypeString(), object.getName());
        title.setText(object.getName());
        type.setText(object.getTypeString());

        title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rename();
            }
        });

    }


    private void rename(){
        RenameDialog dialog = new RenameDialog(ObjectActivity.this,this,object.getTypeString(), object.getName());
        dialog.show();
    }

    private Fragment selectCorrectFragment(ConnectedObject connectedObject){

        Fragment fragment = null;

        switch (connectedObject.getType()){
            case ConnectedObject.LIGHT:
                fragment = new LightFragment();
                break;

            case ConnectedObject.OUTLET:
                fragment = new OutletFragment();
                break;

            case ConnectedObject.TEXTTOSPOEECH:
                fragment = new TexttospeechFragment();
                break;

            case ConnectedObject.MUSIC:
                fragment = new MusicFragment();
                break;

            default:
                fragment = new LightFragment();
                break;
        }

        return fragment;
    }

    private void setupFragment(Fragment fragment){
        Bundle data = new Bundle();//Use bundle to pass data
        //data.putString("type", object.getTypeString());
        data.putString("name", object.getName());//put string, int, etc in bundle with a key value
        fragment.setArguments(data);//Finally set argument bundle to fragment
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.fragment, fragment);
        fragmentTransaction.commit();
    }

    private void setUpImage(String type, String name){
        Glide.with(getApplicationContext())
                .load(getString(R.string.url_image_generic)+type+"&name="+name)
                .into(image);
    }
}
