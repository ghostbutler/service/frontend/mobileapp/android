package com.example.joseph.ghostbutlerapp.dataClasses;

import android.widget.Toast;

import java.io.Serializable;

public class ConnectedObject implements Serializable{

    public static final int GAMEPAD = 0;
    public static final int MUSIC = 1;
    public static final int TEXTTOSPOEECH = 2;
    public static final int LIGHT = 3;
    public static final int OUTLET = 4;
    public static final int MOISTURE = 5;
    public static final int TEMPERATURE = 6;

    private int type;
    private String name;


    public ConnectedObject(int type, String name){
        this.type = type;
        this.name = name;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTypeString(){

        String type = "";

        switch (this.getType()){
            case GAMEPAD:
                type = "gamepad";
                break;

            case LIGHT:
                type = "light";
                break;

            case OUTLET:
                type = "outlet";
                break;

            case MOISTURE:
                type = "moisture";
                break;

            case MUSIC:
                type = "music";
                break;

            case TEXTTOSPOEECH:
                type = "texttospeech";
                break;

            case TEMPERATURE:
                type = "temperature";
                break;
        }

        return type;
    }
}
