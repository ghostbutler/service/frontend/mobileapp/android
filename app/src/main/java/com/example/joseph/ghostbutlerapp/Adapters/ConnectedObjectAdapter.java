package com.example.joseph.ghostbutlerapp.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.joseph.ghostbutlerapp.Activities.ObjectActivity;
import com.example.joseph.ghostbutlerapp.Activities.ObjectsByTypeActivity;
import com.example.joseph.ghostbutlerapp.Dialog.RenameDialog;
import com.example.joseph.ghostbutlerapp.backendServices.ActionSender;
import com.example.joseph.ghostbutlerapp.dataClasses.Action;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.Channel;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.example.joseph.ghostbutlerapp.R;
import com.example.joseph.ghostbutlerapp.dataClasses.ConnectedObject;

import org.json.JSONArray;
import org.json.JSONException;

import java.net.URISyntaxException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ConnectedObjectAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    private final int OBJECT = 0;

    private Context context;
    private ArrayList<ConnectedObject> objects;
    private int position;
    private ObjectsByTypeActivity activity;


    public ConnectedObjectAdapter(ArrayList<ConnectedObject> objects, Context context, RecyclerView recyclerView){
        this.context = context;
        this.objects = objects;
        this.context = context;
        this.activity = activity;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder mainHolder = null;
        if (viewType == OBJECT){
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            View view = inflater.inflate(R.layout.cellule_objet, parent, false);
            mainHolder = new ObjectCelluleHolder(view);

        }

        return mainHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {

        final ConnectedObject currentObject = objects.get(position);

        if(holder instanceof ObjectCelluleHolder){
            ObjectCelluleHolder holder1 = (ObjectCelluleHolder) holder;
            holder1.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                        Intent intent = new Intent(context, ObjectActivity.class);
                        intent.putExtra("object", currentObject);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(intent);



                    /**String message = "";

                    Action action = new Action(currentObject);
                    action.setIdentifier("tts");
                    action.addField("sentence", "");
                    action.addField("language", "fr");
                    action.addField("volume", 100);


                    try {
                        Log.i("ACTIONJSON", action.toJson().toString());
                    } catch (JSONException e) {

                        e.printStackTrace();
                    }

                    ActionSender sender = null;
                    try {
                        sender = new ActionSender();
                        sender.publishMessage(action.toJson().toString());
                        sender.publishToAMQP();
                    } catch (URISyntaxException e) {
                        e.printStackTrace();
                    } catch (NoSuchAlgorithmException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                     **/

                }
            });

            holder1.itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    sendTest(currentObject);
                    return false;
                }
            });

            holder1.display(currentObject);
        }
    }

    @Override
    public int getItemCount() {
        return objects.size();
    }


    @Override
    public int getItemViewType(int position) {
        return OBJECT;
    }

    public void sendTest(final ConnectedObject object){
        RequestQueue requestQueue  = Volley.newRequestQueue(context);
        StringRequest
                stringRequest
                = new StringRequest(
                Request.Method.PUT,
                context.getString(R.string.url_test),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("ERREUR VOLLEY", error.toString());
                    }

                }){
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String> params = new HashMap<String, String>();
                params.put("name", object.getName());
                params.put("type", object.getTypeString());

                return params;
            }
        };
        requestQueue.add(stringRequest);
    }


    public class ObjectCelluleHolder extends RecyclerView.ViewHolder{

        private final TextView objectName;
        private final ImageView objectImage;

        public ObjectCelluleHolder(View itemView) {
            super(itemView);

            objectName = itemView.findViewById(R.id.object_name);
            objectImage = itemView.findViewById(R.id.object_image);

        }

        public void display(ConnectedObject connectedObject){

            String textToDisplay = connectedObject.getName().substring(0, 1).toUpperCase() + connectedObject.getName().substring(1);
            String textToDisplay2 = textToDisplay.replaceAll("\\d+", "").replaceAll("(.)([A-Z])", "$1 $2");
            objectName.setText(textToDisplay2);

            Log.i("IMAGE URL", context.getString(R.string.url_image_generic)+connectedObject.getTypeString()+"&name="+connectedObject.getName());

            Glide.with(context)
                    .load(context.getString(R.string.url_image_generic)+connectedObject.getTypeString()+"&name="+connectedObject.getName())
                    .into(objectImage);

            switch (connectedObject.getType()){
                case ConnectedObject.LIGHT:

                    break;

                case ConnectedObject.MUSIC:
                    break;

                case ConnectedObject.GAMEPAD:
                    break;

                case ConnectedObject.TEXTTOSPOEECH:
                    break;

                case ConnectedObject.OUTLET:
                    break;
            }
        }
    }

}
