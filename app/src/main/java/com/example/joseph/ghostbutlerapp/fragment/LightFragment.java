package com.example.joseph.ghostbutlerapp.fragment;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.Switch;
import android.widget.Toast;

import com.example.joseph.ghostbutlerapp.R;
import com.example.joseph.ghostbutlerapp.backendServices.ActionSender;
import com.example.joseph.ghostbutlerapp.dataClasses.Action;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;

import org.json.JSONException;

import java.net.URISyntaxException;
import java.security.NoSuchAlgorithmException;

public class LightFragment extends Fragment {

    private ImageView gradient;
    private String name;
    private int previousSomme = 0;
    private Switch onoff;
    private Button blink;
    private SeekBar seekbar;

    @SuppressLint("ClickableViewAccessibility")
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_lampe, container, false);

        name = getArguments().getString("name");

        gradient = rootView.findViewById(R.id.gradient);
        onoff = rootView.findViewById(R.id.onoff);
        blink = rootView.findViewById(R.id.blink);
        seekbar = rootView.findViewById(R.id.brightness);

        seekbar.setMax(25);
        seekbar.setProgress(20);


        gradient.setDrawingCacheEnabled(true);
        gradient.buildDrawingCache(true);

        onoff.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(!b){
                    changeLightState("off");
                }else{
                    changeLightState("on");
                }
            }
        });

        gradient.setOnTouchListener(new View.OnTouchListener(){
            @Override
            public boolean onTouch(View v, MotionEvent event){

                if(event.getAction() == MotionEvent.ACTION_DOWN){
                    Bitmap bitmap = gradient.getDrawingCache();
                    int pixel = bitmap.getPixel((int) event.getX(), (int) event.getY());

                    changeColor(Color.red(pixel),Color.green(pixel),Color.blue(pixel));


                }else if(event.getAction() == MotionEvent.ACTION_MOVE){

                    Bitmap bitmap = gradient.getDrawingCache();
                    int pixel = bitmap.getPixel((int) event.getX(), (int) event.getY());

                    int somme = Color.red(pixel) + Color.green(pixel) + Color.blue(pixel);


                    if(Math.abs((previousSomme + somme) - previousSomme*2) > 15){
                        changeColor(Color.red(pixel),Color.green(pixel),Color.blue(pixel));
                    }

                    previousSomme = somme;



                }

                return true;
            }
        });

        seekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                setBrightness(i*10);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        blink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                makeBlink();
            }
        });


        return rootView;
    }

    private void changeLightState(String value){
        Action action = new Action("light", name);
        action.setIdentifier(value);

        try {
            ActionSender sender = new ActionSender(getString(R.string.rabbitmq_address));
            sender.publishMessage(action.toJson().toString());
            sender.publishToAMQP();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void makeBlink(){
        Action action = new Action("light", name);
        action.setIdentifier("blink");

        try {
            ActionSender sender = new ActionSender(getString(R.string.rabbitmq_address));
            sender.publishMessage(action.toJson().toString());
            sender.publishToAMQP();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void changeColor(int red, int green, int blue){
        Action action = new Action("light", name);
        action.setIdentifier("color");
        action.addField("color", "r="+String.valueOf(red)+"&g="+String.valueOf(green)+"&b="+String.valueOf(blue));

        try {
            ActionSender sender = new ActionSender(getString(R.string.rabbitmq_address));
            sender.publishMessage(action.toJson().toString());
            sender.publishToAMQP();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void setBrightness(int brightness){
        Action action = new Action("light", name);
        action.setIdentifier("brightness");
        action.addField("brightness", brightness);

        try {
            ActionSender sender = new ActionSender(getString(R.string.rabbitmq_address));
            sender.publishMessage(action.toJson().toString());
            sender.publishToAMQP();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

}
