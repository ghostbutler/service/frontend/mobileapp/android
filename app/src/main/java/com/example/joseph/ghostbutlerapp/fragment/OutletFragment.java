package com.example.joseph.ghostbutlerapp.fragment;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.Toast;

import com.example.joseph.ghostbutlerapp.R;
import com.example.joseph.ghostbutlerapp.backendServices.ActionSender;
import com.example.joseph.ghostbutlerapp.dataClasses.Action;

import org.json.JSONException;

import java.net.URISyntaxException;
import java.security.NoSuchAlgorithmException;

public class OutletFragment extends Fragment {

    private Switch onoff;
    private String name;

    @SuppressLint("ClickableViewAccessibility")
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_outlet, container, false);

        name = getArguments().getString("name");

        onoff = rootView.findViewById(R.id.onoff);

        onoff.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(!b){
                    changeOutletState("off");
                }else{
                    changeOutletState("on");
                }
            }
        });


        return rootView;
    }



    private void changeOutletState(String value){
        Action action = new Action("outlet", name);
        action.setIdentifier(value);

        try {
            ActionSender sender = new ActionSender(getString(R.string.rabbitmq_address));
            sender.publishMessage(action.toJson().toString());
            sender.publishToAMQP();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

}
