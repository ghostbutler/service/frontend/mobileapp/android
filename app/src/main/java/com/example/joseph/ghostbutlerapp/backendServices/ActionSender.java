package com.example.joseph.ghostbutlerapp.backendServices;

import android.util.Log;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.net.URISyntaxException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
;
import java.util.concurrent.BlockingDeque;
import java.util.concurrent.LinkedBlockingDeque;

public class ActionSender {

    private BlockingDeque queue;
    private ConnectionFactory factory;
    private Thread publishThread;

    private String rabbitMQHostname;

    public ActionSender(String rabbitMQHostname) throws URISyntaxException, NoSuchAlgorithmException {
        queue = new LinkedBlockingDeque();
        factory = new ConnectionFactory();
        this.rabbitMQHostname = rabbitMQHostname;
        setupConnectionFactory();
    }

    public void publishMessage(String message){
        try {
            queue.putLast(message);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void setupConnectionFactory() throws NoSuchAlgorithmException, URISyntaxException {
        factory.setAutomaticRecoveryEnabled(false);
        factory.setHost(this.rabbitMQHostname);
        factory.setPort(5672);
        factory.setUsername("ghost");
        factory.setPassword("ghost");
    }

    public void publishToAMQP(){
        publishThread = new Thread(new Runnable() {
            @Override
            public void run() {
                while(true) {
                    try {
                        Connection connection = factory.newConnection();
                        Channel ch = connection.createChannel();
                        ch.confirmSelect();

                        while (true) {
                            String message = queue.takeFirst().toString();
                            try{
                                ch.basicPublish("", "action", null, message.getBytes());
                                ch.waitForConfirmsOrDie();
                                Log.i("message sended","yes");
                                connection.close();
                            } catch (Exception e){
                                queue.putFirst(message);
                                throw e;
                            }
                        }
                    } catch (InterruptedException e) {
                        break;
                    } catch (Exception e) {
                        Log.d("", "Connection broken: " + rabbitMQHostname + ", " + e.getClass().getName());
                        try {
                            Thread.sleep(5000); //sleep and then try again
                        } catch (InterruptedException e1) {
                            break;
                        }
                    }
                }
            }
        });
        publishThread.start();
    }

}
