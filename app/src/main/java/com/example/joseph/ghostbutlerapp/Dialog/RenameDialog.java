package com.example.joseph.ghostbutlerapp.Dialog;


import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.joseph.ghostbutlerapp.R;
import com.example.joseph.ghostbutlerapp.backendServices.ActionSender;
import com.example.joseph.ghostbutlerapp.dataClasses.Action;

import org.json.JSONException;

import java.net.URISyntaxException;
import java.security.NoSuchAlgorithmException;


public class RenameDialog extends Dialog {

    private Context context;
    private Activity activity;
    private String type;
    private String name;
    private EditText newName;
    private Button confirm;

    public RenameDialog(Context context, Activity activity, String type, String name){
        super(context);
        this.context = context;
        this.activity = activity;
        this.type = type;
        this.name = name;
        this.init();
    }

    private void init(){
        this.setContentView(R.layout.dialog_rename);
        this.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        newName = findViewById(R.id.edit_name);
        confirm = findViewById(R.id.confirm);

        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changeName(type, name, newName.getText().toString());
                Toast.makeText(activity, "Requete envoyée",Toast.LENGTH_LONG).show();
                RenameDialog.this.dismiss();
            }
        });


    }

    private void changeName(String type, String name, String newName){
        Action action = new Action(type, name);
        action.setIdentifier("name");
        action.addField("name", newName);

        try {
            ActionSender sender = new ActionSender(getContext().getString(R.string.rabbitmq_address));
            sender.publishMessage(action.toJson().toString());
            sender.publishToAMQP();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

}
