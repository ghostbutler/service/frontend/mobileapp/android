package com.example.joseph.ghostbutlerapp.Adapters;


import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.example.joseph.ghostbutlerapp.Activities.ControlActivity;
import com.example.joseph.ghostbutlerapp.R;
import com.example.joseph.ghostbutlerapp.dataClasses.ConnectedObject;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class TypesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    private final int OBJECT = 0;

    private Context context;
    private ArrayList<String> types;
    private int position;

    public TypesAdapter(ArrayList<String> types, Context context, RecyclerView recyclerView){
        this.context = context;
        this.types = types;
        this.context = context;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder mainHolder = null;
        if (viewType == OBJECT){
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            View view = inflater.inflate(R.layout.cellule_objet, parent, false);
            mainHolder = new TypeCelluleHolder(view);

        }

        return mainHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {
        final String currentType = types.get(position);

        if(holder instanceof TypeCelluleHolder){
            TypeCelluleHolder holder1 = (TypeCelluleHolder) holder;
            holder1.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    int type = 0;

                    switch (currentType){
                        case "light":
                            type = ConnectedObject.LIGHT;
                            break;

                        case "music":
                            type = ConnectedObject.MUSIC;
                            break;

                        case "texttospeech":
                            type = ConnectedObject.TEXTTOSPOEECH;
                            break;

                        case "outlet":
                            type = ConnectedObject.OUTLET;
                            break;

                        case "gamepad":
                            type = ConnectedObject.GAMEPAD;
                            break;

                        case "moisture":
                            type = ConnectedObject.MOISTURE;
                            break;

                        case "temperature":
                            type = ConnectedObject.TEMPERATURE;
                            break;
                    }

                    Intent intent = new Intent(context, ControlActivity.class);
                    intent.putExtra("type", type);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);
                }
            });

            holder1.display(currentType);
        }
    }

    @Override
    public int getItemCount() {
        return types.size();
    }


    @Override
    public int getItemViewType(int position) {
        return OBJECT;
    }


    public class TypeCelluleHolder extends RecyclerView.ViewHolder{

        private final TextView objectName;
        private final ImageView objectImage;

        public TypeCelluleHolder(View itemView) {
            super(itemView);

            objectName = itemView.findViewById(R.id.object_name);
            objectImage = itemView.findViewById(R.id.object_image);

        }

        public void display(String type){

            objectName.setText(type.substring(0, 1).toUpperCase() + type.substring(1));

            Glide.with(context)
                    .load(context.getString(R.string.url_image_generic)+type)
                    .into(objectImage);
        }
    }

}
