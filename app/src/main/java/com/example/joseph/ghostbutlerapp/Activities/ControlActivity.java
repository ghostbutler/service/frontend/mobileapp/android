package com.example.joseph.ghostbutlerapp.Activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.joseph.ghostbutlerapp.Adapters.ConnectedObjectAdapter;
import com.example.joseph.ghostbutlerapp.R;
import com.example.joseph.ghostbutlerapp.backendServices.ObjectGetter;
import com.example.joseph.ghostbutlerapp.dataClasses.ConnectedObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ControlActivity extends Activity {

    RequestQueue requestQueue;

    private RecyclerView recyclerView;
    private ObjectGetter getter;
    private ProgressBar progressBar;
    private TextView title;


    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_control2);

        recyclerView = findViewById(R.id.recycler_objects);
        progressBar = findViewById(R.id.progress);
        title = findViewById(R.id.title);

        //Toast.makeText(getApplicationContext(),String.valueOf(getIntent().getIntExtra("type", 100)), Toast.LENGTH_LONG).show();
        getter = new ObjectGetter(getIntent().getIntExtra("type",100),getApplicationContext(), this);
        getter.getObjects();

    }


    public void displayList(ArrayList<ConnectedObject> objects){
        try{
            title.setText("Controle des objets de type "+objects.get(0).getTypeString());
            ConnectedObjectAdapter adapter = new ConnectedObjectAdapter(objects,getApplicationContext(), recyclerView);
            LinearLayoutManager manager = new LinearLayoutManager(getApplicationContext());
            recyclerView.setLayoutManager(manager);
            recyclerView.setAdapter(adapter);
            progressBar.setVisibility(View.INVISIBLE);
        }catch (Exception e){
            //TODO
        }

    }

    public void openLampe(){

        Intent intent = new Intent(getApplicationContext(), ObjectActivity.class);
        intent.putExtra("ObjectType", "lampe");
        startActivity(intent);

    }

    public void openPrise(){
        Intent intent = new Intent(getApplicationContext(), ObjectActivity.class);
        intent.putExtra("ObjectType", "prise");
        startActivity(intent);
    }

    public void openCafe(){
        Intent intent = new Intent(getApplicationContext(), ObjectActivity.class);
        intent.putExtra("ObjectType", "cafe");
        startActivity(intent);

    }

}
