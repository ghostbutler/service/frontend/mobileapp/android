package com.example.joseph.ghostbutlerapp.Activities;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.joseph.ghostbutlerapp.Adapters.TypesAdapter;
import com.example.joseph.ghostbutlerapp.R;
import com.example.joseph.ghostbutlerapp.dataClasses.ConnectedObject;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

public class TypesActivity extends Activity {

    private RecyclerView recyclerView;

    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_types);

        recyclerView = findViewById(R.id.recycler);

        RequestQueue requestQueue  = Volley.newRequestQueue(getApplicationContext());
        final ArrayList<String> types = new ArrayList<>();

        StringRequest
                stringRequest
                = new StringRequest(
                Request.Method.GET,
                getString(R.string.url_types),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONArray json  = new JSONArray(response);
                            for (int i =0 ; i < json.length(); i++){
                                types.add(json.getString(i));
                            }

                            TypesAdapter adapter = new TypesAdapter(types, getApplicationContext(), recyclerView);

                            LinearLayoutManager manager = new LinearLayoutManager(getApplicationContext());

                            recyclerView.setLayoutManager(manager);
                            recyclerView.setAdapter(adapter);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("ERREUR VOLLEY", error.toString());
                    }

                }){
        };
        requestQueue.add(stringRequest);

    }


}
