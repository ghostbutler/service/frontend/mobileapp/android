package com.example.joseph.ghostbutlerapp.dataClasses;

import com.example.joseph.ghostbutlerapp.backendServices.ObjectGetter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class Action {

    private String name, description;
    private String objectType;
    private String objectName;
    private String identifier;
    private HashMap<String, Object> fields;


    public Action(){
        this.name = "name";
        this.description = "description";
        this.fields = new HashMap<>();
    }

    public Action(ConnectedObject connectedObject){
        this.name = "name";
        this.description = "description";
        this.objectName = connectedObject.getName();
        this.objectType = connectedObject.getTypeString();
        this.fields = new HashMap<>();
    }

    public Action(String type, String name){
        this.name = "RunFromMobileApp";
        this.description = "This is an example from mobile control";
        this.objectName = name;
        this.objectType = type;
        this.fields = new HashMap<>();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getObjectType() {
        return objectType;
    }

    public void setObjectType(String objectType) {
        this.objectType = objectType;
    }

    public String getObjectName() {
        return objectName;
    }

    public void setObjectName(String objectName) {
        this.objectName = objectName;
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }


    public HashMap<String, Object> getFields() {
        return fields;
    }

    public void setFields(HashMap<String, Object> fields) {
        this.fields = fields;
    }

    public void addField(String fieldName, Object fieldValue){
        this.fields.put(fieldName, fieldValue);
    }

    public JSONObject toJson() throws JSONException {
        JSONObject json = new JSONObject();
        JSONObject jsonGeneral = new JSONObject();
        JSONArray array = new JSONArray();
        JSONObject action = new JSONObject();
        JSONObject field = new JSONObject();
        JSONArray actionArray = new JSONArray();

        action.put("name", objectName);
        action.put("identifier", identifier);

        Iterator it = fields.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry)it.next();
            //System.out.println(pair.getKey() + " = " + pair.getValue());
            field.put(pair.getKey().toString(), pair.getValue());
        }


        action.put("field", field);

        actionArray.put(action);

        json.put("type", objectType);
        json.put("action", actionArray);

        array.put(json);
        jsonGeneral.put("name", name);
        jsonGeneral.put("description", description);
        jsonGeneral.put("action", array);

        return jsonGeneral;
    }
}
