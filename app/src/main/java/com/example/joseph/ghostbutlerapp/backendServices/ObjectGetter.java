package com.example.joseph.ghostbutlerapp.backendServices;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.joseph.ghostbutlerapp.Activities.ControlActivity;
import com.example.joseph.ghostbutlerapp.Adapters.ConnectedObjectAdapter;
import com.example.joseph.ghostbutlerapp.R;
import com.example.joseph.ghostbutlerapp.dataClasses.ConnectedObject;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

public class ObjectGetter {

    private int type;
    private Context context;
    private ControlActivity activity;

    public ObjectGetter(int type, Context context, ControlActivity activity){
        this.type = type;
        this.context = context;
        this.activity = activity;
    }

    public void getObjects(){

        RequestQueue requestQueue  = Volley.newRequestQueue(context);
        final ArrayList<ConnectedObject> objects = new ArrayList<>();

        StringRequest
                stringRequest
                = new StringRequest(
                Request.Method.GET,
                getUrl(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONArray json  = new JSONArray(response);
                            for (int i =0 ; i < json.length(); i++){
                                ConnectedObject object = new ConnectedObject(type, json.getString(i));
                                objects.add(object);
                            }
                            activity.displayList(objects);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("ERREUR VOLLEY", error.toString());
                    }

                }){
        };
        requestQueue.add(stringRequest);

    }


    private String getUrl(){

        String url = null;

        switch (type){
            case ConnectedObject.GAMEPAD:
                url = context.getString(R.string.url_gamepad);
            break;

            case ConnectedObject.MUSIC:
                url =  context.getString(R.string.url_music);
            break;

            case ConnectedObject.LIGHT:
                url =  context.getString(R.string.url_light);
            break;

            case ConnectedObject.OUTLET:
                url = context.getString(R.string.url_outlet);
            break;

            case ConnectedObject.TEXTTOSPOEECH:
                url = context.getString(R.string.url_texttospeech);
            break;

            case ConnectedObject.MOISTURE:
                url = context.getString(R.string.url_moisture);
                break;



            case ConnectedObject.TEMPERATURE:
                url = context.getString(R.string.url_temperature);
                break;

        }


        return url;
    }
}
