package com.example.joseph.ghostbutlerapp.fragment;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import android.widget.Toast;

import com.example.joseph.ghostbutlerapp.R;
import com.example.joseph.ghostbutlerapp.backendServices.ActionSender;
import com.example.joseph.ghostbutlerapp.dataClasses.Action;

import org.json.JSONException;

import java.net.URISyntaxException;
import java.security.NoSuchAlgorithmException;

public class TexttospeechFragment extends Fragment {

    private SeekBar bar;
    private EditText sentence;
    private Button sendButton;
    private RadioGroup language;
    private RadioButton selectedLanguage;
    private String name;
    private int volume;


    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_texttospeech, container, false);

        name = getArguments().getString("name");

        sentence = rootView.findViewById(R.id.sentence);
        sendButton = rootView.findViewById(R.id.go_button);
        language = rootView.findViewById(R.id.languageRadioGroup);
        bar = rootView.findViewById(R.id.volume);

        bar.setMax(100);
        bar.setProgress(90);

        bar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                volume = i;
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int selected = language.getCheckedRadioButtonId();
                selectedLanguage = rootView.findViewById(selected);
                send();
            }
        });


        return rootView;
    }

    private String convertLanguage( String name ) {
        if( name.equals( getString( R.string.language_english ) ) ) {
            return "en";
        } else if( name.equals( getString( R.string.language_deutch ) ) ) {
            return "de";
        } else if( name.equals( getString( R.string.language_italian ) ) ) {
            return "it";
        } else if( name.equals( getString( R.string.language_spanish ) ) ) {
            return "es";
        } else if( name.equals( getString( R.string.language_chinese ) ) ) {
            return "zh";
        } else if( name.equals( getString( R.string.language_quebec ) ) ) {
            return "fr-CA";
        } else if( name.equals( getString( R.string.language_japanese ) ) ) {
            return "ja";
        } else {
            return "fr";
        }
    }

    private void send(){
        String toSpeech = sentence.getText().toString();
        String lang = convertLanguage( selectedLanguage.getText().toString() );

        Action action = new Action("texttospeech", name);
        action.setIdentifier("tts");
        action.addField("sentence", toSpeech);
        action.addField("language", lang);
        action.addField("volume", volume);

        try {
            ActionSender sender = new ActionSender(getString(R.string.rabbitmq_address));
            sender.publishMessage(action.toJson().toString());
            sender.publishToAMQP();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        //Toast.makeText(getActivity(), selectedLanguage.getText(), Toast.LENGTH_LONG).show();

    }

}
